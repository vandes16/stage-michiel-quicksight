#!/bin/bash
export STACK_NAME=iot-parking-quicksight-api
export MY_REGION=eu-west-1
export MY_DEV_BUCKET=samuel-dev-bucket

# Package new cloudformation package
aws cloudformation package --template template.yaml --s3-bucket $MY_DEV_BUCKET --output-template cfn-template-export-dev.yaml --region eu-west-1
# Deploy 
sam deploy --region eu-west-1 --template-file cfn-template-export-dev.yaml --stack-name $STACK_NAME --capabilities CAPABILITY_NAMED_IAM --parameter-overrides Stage=dev