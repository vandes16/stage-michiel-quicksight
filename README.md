# IoT Parking Quicksight API

Solution based on:
https://github.com/aws-samples/amazon-quicksight-embedding-sample


lambda/fn-getDashboardEmbedUrl/index.js
=> This is the lambda function generating the dashboard url 

web/index.html
=> This is the front-end displaying the dashboard. NOT YET IN REACT

cognito/QuickSightEmbeddingCognito.yaml
=> This is the cloudformation file containing the cognito configuration

## Local Testing

```bash

sam local invoke FnGetDashboardEmbedUrl --event ./lambda/fn-getDashboardEmbedUrl/__tests__/input-question1.json --region eu-central-1 --skip-pull-image 

```

Test voting

```bash
wscat -c wss://e5cifz6vaj.execute-api.eu-central-1.amazonaws.com/prod
{"action": "vote", "message": "{\"questionId\":\"3\",\"choiceId\":\"10\"}" }

```
